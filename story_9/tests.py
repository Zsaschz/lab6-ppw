from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.auth.models import User
from django.urls import resolve, reverse

# Create your tests here.
from . import views

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_url_exists(self):
        response = self.client.get("/story_9/")
        self.assertEqual(response.status_code,200)
        
        response = self.client.get("/story_9/register/")
        self.assertEqual(response.status_code,200)

        response = self.client.get("/story_9/logout/")
        self.assertEqual(response.status_code,302)

        response = self.client.get("/story_9/login/")
        self.assertEqual(response.status_code,302)
    
class ViewsTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('nanda', 'nanda', 'nanda1212')
        self.user.save()

    def test_page_using_correct_views_method(self):
        response = resolve("/story_9/")
        self.assertEqual(response.func,views.index)

        response = resolve("/story_9/register/")
        self.assertEqual(response.func,views.register)

        response = resolve("/story_9/logout/")
        self.assertEqual(response.func,views.logOut)

    def test_model(self):
        total = User.objects.all().count()
        self.assertEqual(total,1)

    
    def test_register_test(self):
        response = self.client.post('/story_9/register/', data={'username' : 'nanda12', 'email':'nanda', 'password' : 'nanda121212'})
        self.assertEqual(response.status_code, 302)