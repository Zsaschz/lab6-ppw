from os import name, pardir
from django.urls import path
from . import views

app_name = 'story_9'

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.logIn, name='login'),
    path('logout/', views.logOut, name='logout'),
    path('register/', views.register, name='register')
]
