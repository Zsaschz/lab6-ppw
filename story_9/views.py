from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
from .forms import CreatUserForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User


# Create your views here.
def index(request):
    return render(request, 'story_9.html')

@login_required(login_url='/admin/login/')
def logIn(request):
    return redirect("story_9:index")

def logOut(request):
    logout(request)
    return redirect("story_9:index")

def register(request):

    if request.method == 'POST':
        form = CreatUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = User.objects.get(username=form.cleaned_data.get('username'))
            user.is_superuser = True
            user.is_staff = True
            user.save()

            loggedIn = authenticate(request, username=form.cleaned_data.get('username'), password=form.cleaned_data.get('password1'))
            login(request,loggedIn)

        return redirect('story_9:index')

    form = CreatUserForm()
    return render(request, 'story_9_register.html', {'form':form})