from os import name, pardir
from django.urls import path
from . import views

app_name = 'story_8'

urlpatterns = [
    path('', views.index, name='index'),
    path('<keyword>', views.get_book, name="get_book")
]
