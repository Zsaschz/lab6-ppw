# from django.forms import models
from . import models
from django.http import request
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from .models import Kegiatan, Orang
from .forms import Input_Form, Orang_Form
# Create your views here.


def index(request):
    form = Input_Form(request.POST)
    orang_form = Orang_Form(request.POST)

    if form.is_valid() and request.method == 'POST':
        data = form.cleaned_data
        new_kegiatan = Kegiatan()
        new_kegiatan.nama = data['nama_kegiatan']
        new_kegiatan.deskripsi = data['deskripsi']
        new_kegiatan.save()

    elif orang_form.is_valid() and request.method == 'POST':
        data = orang_form.cleaned_data
        print(request.POST['id_kegiatan'])
        kegiatan = Kegiatan.objects.get(id=request.POST['id_kegiatan'])
        new_orang = Orang()
        new_orang.nama = data['nama_orang']
        new_orang.ikut = kegiatan
        new_orang.save()

    list_kegiatan = Kegiatan.objects.all()
    list_kegiatan_orang = []
    for kegiatan in list_kegiatan:
        list_orang = Orang.objects.filter(ikut=kegiatan)
        data_orang = []
        for orang in list_orang:
            data_orang.append(orang)
        list_kegiatan_orang.append((kegiatan, data_orang))

    return render(request, 'list_kegiatan.html', {'kegiatans': list_kegiatan_orang, 'orang_form': Orang_Form})


def create(request):
    return render(request, 'form.html', {'form': Input_Form})


def delete(request, id):
    kegiatan_to_delete = Kegiatan.objects.get(id=id)
    Kegiatan.delete(kegiatan_to_delete)
    return redirect('kegiatan:list')
