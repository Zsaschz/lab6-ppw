from django.db import models
from django.db.models.fields.related import ForeignKey

# Create your models here.


class Kegiatan(models.Model):
    nama = models.CharField(max_length=50, default="")
    deskripsi = models.CharField(max_length=100, default="")


class Orang(models.Model):
    nama = models.CharField(max_length=50, default="")
    ikut = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
