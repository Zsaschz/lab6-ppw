from django import forms


class Input_Form(forms.Form):
    nama_kegiatan = forms.CharField(
        label='Nama Kegiatan', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))
    deskripsi = forms.CharField(label='Deskripsi', max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control'}))


class Orang_Form(forms.Form):
    nama_orang = forms.CharField(
        label='Nama Kegiatan', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))
