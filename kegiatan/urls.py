from os import name, pardir
from django.urls import path
from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('', views.index, name='list'),
    path('new/', views.create, name='tambah'),
    path('delete/<int:id>', views.delete, name='delete'),
]
