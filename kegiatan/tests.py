from django.http import response
from django.test import TestCase, Client
from .models import *

# Create your tests here


class Test(TestCase):
    def test_model_kegiatan(self):
        Kegiatan.objects.create(nama='MABAR', deskripsi="MABAR KUY")
        hitung = Kegiatan.objects.all().count()
        self.assertEquals(hitung, 1)

    def test_model_orang(self):
        Orang.objects.create(nama='Fathan', ikut=Kegiatan.objects.create(
            nama='MABAR', deskripsi="MABAR KUY"))
        hitung_orang = Orang.objects.all().count()
        self.assertEquals(hitung_orang, 1)

    def test_url_exist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_url_new_exist(self):
        response = Client().get('/new/')
        self.assertEquals(response.status_code, 200)

    def test_POST_kegiatan(self):
        response = Client().post(
            '/', data={'nama_kegiatan': 'MABAR', 'deskripsi': 'MABAR KUY'})
        hitung = Kegiatan.objects.filter(nama='MABAR').count()
        self.assertEquals(hitung, 1)

    def test_POST_orang(self):
        Kegiatan.objects.create(nama="MABAR", deskripsi="MABAR KUY")
        id = Kegiatan.objects.filter(nama="MABAR")[0].id
        response = Client().post(
            '/', data={'nama_orang': "Fathan", 'id_kegiatan': id})
        hitung = Orang.objects.filter(nama='Fathan').count()
        self.assertEqual(hitung, 1)

    def test_delete_kegiatan(self):
        Kegiatan.objects.create(nama="MABAR", deskripsi="MABAR KUY")
        id = Kegiatan.objects.filter(nama="MABAR")[0].id
        response = Client().get('/delete/' + str(id))
        hitung = Kegiatan.objects.all().count()
        self.assertEquals(hitung, 0)
